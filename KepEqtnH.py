# -*- coding: utf-8 -*-
#
# ALgoritmo 3 - Vallado
#
# Purpose:  Solutions of Kepler?s Equation - Parabolic Solution
# Author:   Nicol?s Conde
# Creation Date:  Oct. 8, 2018
#

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

from Parameters import TOLERANCE

def KepEqtnH(M, e):
    """
    Main algorithm - Hyperbolic Solution of Kepler Equation

    Args:
        M: Mean anomaly [rad]
        e: eccentricity
        abstol: tolerance for convergence

    Outputs:
        H: Hyperbolic anomaly [rad]
        v: true anomaly [rad]
        n_it: number of iterations until convergence
    """
    M *= np.pi/180

    if e < 1.6:
        if M > np.pi or M < 0 and M > -np.pi:
            H = M - e
        else:
            H = M + e
    else:
        if e < 3.6 and np.abs(M) > np.pi:
            H = M - np.sign(M)*e
        else: 
            H = M / (e-1)
    
    H0 = H - 3*TOLERANCE   # comparison value
    n_it = 0  # counter

    while np.abs(H - H0) > TOLERANCE:
            H0 = H
            H += (M - e*np.sinh(H) + H)/(e*np.cosh(H)-1)
            n_it += 1
    nu = np.arcsin((-np.sinh(H)*(e**2-1)**0.5)/(1-e*np.cosh(H)))
    return [H, nu, n_it]

def test_hyperbolic_plot():
    
    """
    Plot M vs v variation

    Outputs:
        plot M vs v
    """
    fig = plt.figure(figsize=(8,6))

    M = np.linspace(-180,180,360)
    e = [2.4]
    
    H = []
    for j in e:
        for m in M:
            H += [KepEqtnH(m, j)]
    H = np.array(H)
    plt.plot(H[:,1]*180/np.pi, M, linewidth=0.8 )
    plt.yticks(np.linspace(-180, 180, 13))   
    plt.xticks(np.linspace(-180, 180, 13))    
    plt.grid()
    plt.title("M vs v (Hyperbolic Solution)")
    plt.ylabel("Mean anomaly")
    plt.xlabel("True anomaly")
    plt.legend()
    plt.show()
    
    return 0

if __name__ == "__main__":
    print(KepEqtnH(235.4, 2.4))
    test_hyperbolic_plot()

