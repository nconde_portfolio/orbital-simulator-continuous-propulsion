#
# Input Parameters for Astrodynamics algorithms
#
# Author:   Nicolás Conde
# Creation Date:  Oct. 16, 2018
#

import numpy as np
import Corroboraciones as Error

### CONSTANTS
REQ_EARTH     = 6378.137                     # Earth equatorial radius         [km]
RB_EARTH      = 6356.7516005                 # Earth semi-minor axis           [km]
MU_EARTH      = 398600.4415                  # Gravitational parameter         [km^3/s^2]
SIDEREAL_DAY  = 86164.0905                   # sidereal day duration           [s]
EARTH_OMEGA   = 2*np.pi/SIDEREAL_DAY         # Earth's angular velocity        [rad/s]
EARTH_OMEGA_V = np.array([0,0,EARTH_OMEGA])  # Earth's angular velocity vector [rad/s]
TROPICAL_YEAR = 365.2421897*24*3600          # tropical year                   [s]
f_EARTH       = 1/298.257223563              # Earth's flattening              [-]
e_EARTH       = np.sqrt(1-(1-f_EARTH)**2)    # Earth's eccentricity            [-]
gEQ_EARTH     = 9.7803253359                 # Earth equatorial gravity        [m/s^2]
TOLERANCE     = 1e-6

def gravity_model(Ogd):
    return gEQ_EARTH * (1 + 0.00193185265241 * np.sin(Ogd)**2) / (1 - e_EARTH**2*np.sin(Ogd)**2)**.5

class vector_class:
    def __init__(self, vec, sys_ref):
        self.coor = vec
        self.abs = np.linalg.norm(self.coor)
        if self.abs > 1e-16:
            self.unit = self.coor / self.abs
        else:
            self.unit = vec
        self.sys = Error.Reference_System_Valid(sys_ref)

    def __str__(self):
        return f" vector: {self.coor} km - Reference System {self.sys}"

        
class coe_class:
    def __init__(self, param_orb):
        self.a = param_orb[0]
        self.e = param_orb[1]
        self.p = param_orb[2]
        self.i = param_orb[3]
        self.w = param_orb[4]
        self.raan = param_orb[5]
        self.v = param_orb[6]
        self.l_t = param_orb[7]
        self.w_t = param_orb[8]
        self.u = param_orb[9]

    def __str__(self):
        return f"\n\t semimajor axis: {self.a} km \n\t eccentricity: {self.e} \n\t semiparameter p: {self.p} km \n\t inclination: {self.i} rad \n\t raan: {self.raan} rad \n\t argument perigee: {self.w} rad \n\t true anomaly: {self.v} rad"

def Vector(vector, sys_reference):
    return vector_class(vector, sys_reference)

def COE(param_orb):
    return coe_class(param_orb)

if __name__ == "__main__":
    print("Nothing to see here")