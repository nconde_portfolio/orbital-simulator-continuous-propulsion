# -*- coding: utf-8 -*-
#
# Algoritmo 11 - Vallado
#
# Purpose: determine the amount of time between two locations on an orbit 
# Author:   Nicol?s Conde
# Creation Date:  Dec. 12, 2021
#

import numpy as np

from Parameters import MU_EARTH, TOLERANCE, Vector
from keplerian_orbit_propagator import KeplerianRungeKuttaPropagator
from Coe2rv import RV2COE, COE2RV

def findTOF(r0, r, p, mu=MU_EARTH, tol=TOLERANCE):
    """
    Determine the amount of time between two locations on an orbit

    Args:
        r0: initial position vector [km]
        r: final position vector [km]
        p: semiparameter [km]
        mu: gravitational parameter of the planet
        tol: tolerance for convergence

    Outputs:
        TOF: time of flight [sec]
    """
    cos_dnu = np.dot(r0.coor, r.coor)/r.abs/r0.abs
    sen_dnu = (1-cos_dnu**2)**0.5
    tan_dnu = sen_dnu/cos_dnu
    
    k = r0.abs * r.abs * (1 - cos_dnu)
    m = r0.abs * r.abs * (1 + cos_dnu)
    l = r.abs + r0.abs

    a = (m*k*p)/((2*m-l**2)*p**2+2*k*l*p-k**2)

    f = 1 - r.abs/p*(1-cos_dnu)
    g = r0.abs*r.abs*sen_dnu/(mu*p)**0.5

    if a > 1e-6 and a < 1e47:
        dE = np.arccos(1-r0.abs/a*(1-f))
        return g + (a**3/mu)**0.5*(dE-np.sin(dE))
    elif a < -1e-6:
        dH = np.arccosh(1+(f-1)*r0.abs/a)
        return g + (-a**3/mu)**0.5*(np.sinh(dH)-dH)
    elif a == float('inf'):
        c = (r.abs**2+r0.abs**2-2*r.abs*r0.abs*cos_dnu)**.5
        s = (r0.abs+r.abs+c)/2
        return 2/3*(s**3/2/mu)**.5*(1-(1-c/s)**1.5)

def test_findTOF():
    a = 36126 # elliptical

    # COE2RV and RV2COE is not prepared for hyperbolic or parabolic motion
    #a = -5000 # hyperbolic
    #a = float('inf') # parabolic

    r_IJK, v_IJK = COE2RV(a, 0.83, 227.89*np.pi/180, 87.87*np.pi/180, 53.38*np.pi/180, 92.335*np.pi/180)
    X = KeplerianRungeKuttaPropagator(r_IJK, v_IJK, 60*60, 1)

    COE = RV2COE(r_IJK, v_IJK)

    r_final = Vector(X[-1, :3], "ECEF")

    TOF = findTOF(r_IJK, r_final, COE.p)

    print("TOF is ", 60*60, " sec\n")
    print("TOF calculated is ", TOF, " sec\n")

if __name__ == "__main__":
    test_findTOF()