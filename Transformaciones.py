# -*- coding: utf-8 -*-
#
# Coordinates transformation
#
# Purpose:  Classic transformations between coordinate reference systems
# Author:   Nicolás Conde
# Creation Date:  Oct. 9, 2018
#
import numpy as np

import Rotaciones as rot
from Parameters import e_EARTH, MU_EARTH, REQ_EARTH, TOLERANCE, Vector

def Ogd2Ogc(Ogd):
    """
    Geodetic to geocentric transformation (if requires accuracy, implement. Now, I assume identical)

    Args:
        Ogd: Geodetic latitude [rad]

    Output:
        Ogc: Geocentric latitude [rad]
    """
    return Ogd

def LATLON2ECI(Ogd, Olst, Hmsl):
    """
    Site coordinates for the observatory on ECI reference system

    Args:
        Ogd: geodetic latitude [rad]
        Olst: longitude [rad]
        Hmsl: height above the ellipsoid [km]

    outputs:
        Vsite: position vector of the site on ECI [km]
    """
    C_EARTH = REQ_EARTH / ( 1 - (e_EARTH * np.sin(Ogd))**2 )**.5
    S_EARTH = REQ_EARTH * (1-e_EARTH**2) / ( 1 - (e_EARTH * np.sin(Ogd))**2 )**.5
    
    v1 = (C_EARTH + Hmsl) * np.cos(Ogd)
    v3 = (S_EARTH + Hmsl) * np.sin(Ogd)

    Vsite = (v1**2+v3**2)**.5

    Ogc = Ogd2Ogc(Ogd)

    return Vector(np.array([Vsite*np.cos(Ogc)*np.cos(Olst),
                            Vsite*np.cos(Ogc)*np.sin(Olst),
                            Vsite*np.sin(Ogc),])
                  ,"ECEF")

def ECI2LATLON(Veci):
    """
    Intertial frame to latitude and longitude

    Args:
        Veci: Vector on ECI frame [km]

    Output:
        h_ellp: Local upper-ellipsoid height [km]
        Olst: Longitude [rad]
        Ogd: Geodetic latitude [rad]
    """
    Vsite = np.sqrt(Veci.coor[0]**2 + Veci.coor[1]**2)
    
    Olst = np.arctan2(Veci.coor[1], Veci.coor[0])
    
    if Olst < 0:
        Olst = Olst + 2*np.pi
    
    Ogd = np.arctan2(Veci.coor[2], Vsite)
    
    Ogd0 = Ogd - TOLERANCE*3
    
    while np.abs(Ogd - Ogd0) > TOLERANCE:
        Ogd0 = Ogd

        C_plus = REQ_EARTH / ( 1 - (e_EARTH * np.sin(Ogd))**2 )**.5
        tan_Ogd = (Veci.coor[2] + C_plus*(e_EARTH**2)*np.sin(Ogd))
      
        Ogd = np.arctan2(tan_Ogd, Vsite)
    
    C_plus = REQ_EARTH / ( 1 - (e_EARTH * np.sin(Ogd))**2 )**.5

    h_ellp = Vsite / np.cos(Ogd) - C_plus
    
    return [h_ellp, Olst, Ogd]

def SEZ2ECI(Vsez, Ogd, Olst):
    """
    SEZ to ECI

    Args:
        Vsez: Vector on SEZ frame [km]
        Ogd: local geodetic latitude [rad]
        Olst: local longitude [rad]

    Output:
        Veci: Vector on ECI frame [km]
    """
    return Vector(rot.ROT3(-Olst)*rot.ROT2(-np.pi/2+Ogd)*Vsez.coor,"ECEF")

def ECI2SEZ(Veci, Ogd, Olst):
    """
    ECI to SEZ

    Args:
        Veci: Vector on ECI frame [km]
        Ogd: local geodetic latitude [rad]
        Olst: local longitude [rad]

    Output:
        Vsez: Vector on SEZ frame [km]
    """
    return Vector(rot.ROT2(np.pi/2-O_gd)*rot.ROT3(Olst)*Veci.coor,"SEZ")

def RAZEL(Vsez): # page 205
    el = np.arctan2(Vsez.coor[2], np.sqrt(Vsez.coor[1]**2 + Vsez.coor[0]**2))
    
    if el == 90:
        az = np.arctan2(Vsez.coor[1], Vsez.coor[0])
    else:
        az = np.arctan2(Vsez.coor[1], Vsez.coor[0])
    
    return el, az

def ECEF2PQW(Veci, w, i, raan):
    """
    ECI to PWQ

    Args:
        Veci: Vector on ECI frame [km]
        w: perigee argument [rad]
        i: orbit inclination [rad]
        raan: raan [rad]

    Output:
        Vpqw: Vector on PQW frame [km]
    """
    Rotacion = np.dot(np.dot(rot.ROT3(w), rot.ROT1(i)), rot.ROT3(raan))
    return Vector(np.dot(Rotacion, Veci.coor), "PQW")

def PQW2ECEF(Vpqw, w, i, raan):
    """
    PQW to ECI

    Args:
        Vpqw: Vector on PQW frame [km]
        w: perigee argument [rad]
        i: orbit inclination [rad]
        raan: raan [rad]

    Output:
        Veci: Vector on ECI frame [km]
    """
    Rotacion = np.dot(np.dot(rot.ROT3(-raan), rot.ROT1(-i)), rot.ROT3(-w))
    return Vector(np.dot(Rotacion, Vpqw.coor), "ECEF")

def PQW2RSW(Vpqw, v):
    """
    PQW to RSW

    Args:
        Vpqw: Vector on PQW frame [km]
        v: true anomaly [rad]

    Output:
        Vrsw: Vector on RSW frame [km]
    """
    return Vector(np.dot(rot.ROT3(v), Vpqw.coor),"RSW")

def RSW2PQW(Vrsw, v):
    """
    RSW to PQW

    Args:
        Vrsw: Vector on RSW frame [km]
        v: true anomaly [rad]

    Output:
        Vpqw: Vector on PQW frame [km]
    """
    return Vector(np.dot(rot.ROT3(-v), Vrsw.coor),"PQW")

def ECI2EQW(Veci, raan, i):
    """
    ECI to EQW

    Args:
        Veci: Vector on ECI frame [km]
        raan: raan [rad]
        i: orbit inclination [rad]

    Output:
        Veqw: Vector on EQW frame [km]
    """
    if i>=0.0 and i<=np.pi/2:
        fr = 1
    elif i<=np.pi and i>=np.pi/2:
        fr = -1

    Rotacion = np.dot(np.dot(rot.ROT1(i), rot.ROT3(raan)), rot.ROT3(-fr*raan))

    return Vector(np.dot(Rotacion, Veci.coor),"EQW")

def EQW2ECI(Veqw, raan, i):
    """
    EQW to ECI

    Args:
        Veqw: Vector on EQW frame [km]
        raan: raan [rad]
        i: orbit inclination [rad]

    Output:
        Veci: Vector on ECI frame [km]
    """
    if i>=0.0 and i<=np.pi/2:
        fr = 1
    elif i<=np.pi and i>=np.pi/2:
        fr = -1

    Rotacion = np.dot(np.dot(rot.ROT1(-i), rot.ROT3(fr*raan)), rot.ROT3(-raan))

    return Vector(np.dot(Rotacion, Veqw.coor),"ECI")

def test_ECI2EQW():
    print("transformation validation with example 2-5")
    Reci = Vector(np.array([6524.834, 6862.875, 6448.296]), "ECEF") # [km]
    Veci = Vector(np.array([4.901327, 5.533756, -1.976341]), "ECEF") # [km/s]
    
    print("\nReci vector")
    print(Reci.__str__())
    print("\nVeci vector")
    print(Veci.__str__())

    raan = 3.9775750028016947
    i = 1.5336055626394494
    
    Reqw = ECI2EQW(Reci, raan, i)
    Veqw = ECI2EQW(Veci, raan, i)
    
    print("\nTo EQW...")

    print("\nReqw vector")
    print(Reqw.__str__())
    print("\nVeci vector")
    print(Veqw.__str__())

    print("\nTo ECI...")
    
    Reci = EQW2ECI(Reqw, raan, i)
    Veci = EQW2ECI(Veqw, raan, i)
    
    print("\nReci vector")
    print(Reci.__str__())
    print("\nVeci vector")
    print(Veci.__str__())

def test_eci_latlon():
    print("Validation with example 3-2")

    Ogd  = -7.9066357*np.pi/180  # [rad]
    Olst = 345.5975*np.pi/180   # [rad]
    Hmsl = 56/1000        # [km]

    r_site = LATLON2ECI(Ogd, Olst, Hmsl)

    print("initial R_ECI")
    print(r_site.__str__())
    print("H_ellp: ", Hmsl)
    print("\nLon: ", Olst*180/np.pi)
    print("\nOgd: ", Ogd*180/np.pi)

    print("\nTo LATLON...\n")
    hellp, Olst, Ogd = ECI2LATLON(r_site)
    print("H_ellp: ", hellp)
    print("\nLon: ", Olst*180/np.pi)
    print("\nOgd: ", Ogd*180/np.pi)

if __name__ == "__main__":
    test_ECI2EQW()
    test_eci_latlon()