# -*- coding: utf-8 -*-
#
# ALgoritmo 5 y 6 - Vallado
#
# Purpose:  Solutions of Kepler’s Equation - True anomaly to anomalies (E, B or H)
# Author:   Nicolás Conde
# Creation Date:  Oct. 8, 2018
#

import numpy as np
import matplotlib.pyplot as plt

def nu2anomalies(nu, e):
    """
    Calculation of anomalies E, H or B (elliptical, hyperbolic, parabolic)

    Args:
        nu: true anomaly [rad]
        e: eccentricity

    Output:
        anomaly: E, H or B [rad]
    """
    if e > -1e-6 and e < 1.0:
        return np.arctan2(np.sin(nu)*np.sqrt(1-e**2), e + np.cos(nu))
    elif e == 1.0:
        return np.tan(nu/2)
    elif e > 1.0:
        return np.arctanh(np.sin(nu)*(e**2-1)**0.5/(e+np.cos(nu)))
    else:
        raise NonEllipticOrbitException
        
def Anomalies2nu(e, E=1, B=1, H=1, p=1, r=1):
    """
    Calculation of anomalies E, H or B (elliptical, hyperbolic, parabolic)

    Args:
        anomaly: E, H or B [rad]
        e: eccentricity

    Output:
        nu: true anomaly [rad]
    """
    if e > -1e-6 and e < 1:
        return np.arctan2(np.sin(E)*np.sqrt(1 - e**2), -e + np.cos(E))
    elif e == 1.0:
        return np.arctan2(p*B,p-r)
    elif e > 1.0:
        return np.arctanh(-np.sinh(H)*(e**2-1)**0.5/(np.cosh(H)-e))
    else:
        raise NonEllipticOrbitException
        
class NonEllipticOrbitException(Exception):
    print("Error e<=0")
    pass

def test_graphic_M_vs_nu():
    nu = np.linspace(-180,180,360)
    e = np.linspace(0,1.4,8)

    for j in e:
        string = 'e = ' + str(j)
        anomalies = nu2anomalies(nu*np.pi/180, j)*180/np.pi
        anomalies[anomalies>180]=180
        anomalies[anomalies<-180]=-180
        plt.plot(nu,anomalies, label=string)
    
    plt.yticks(np.linspace(-180, 180, 13))
    plt.xticks(np.linspace(-180, 180, 13))
    plt.grid()
    plt.title("nu vs anomalies")
    plt.legend()
    plt.show()
    
    return nu

if __name__ == "__main__":
    test_graphic_M_vs_nu()