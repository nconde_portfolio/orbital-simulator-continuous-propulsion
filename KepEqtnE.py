# -*- coding: utf-8 -*-
#
# ALgoritmo 2 - Ejercicio 1 Guía 2 - Vallado
#
# Purpose:  Solutions of Kepler’s Equation - Elliptical Solution
# Author:   Nicolás Conde
# Creation Date:  Oct. 8, 2018
#

import numpy as np
import matplotlib.pyplot as plt

from Parameters import TOLERANCE

def KepEqtnE(M, e):
    """
    Main algorithm - Elliptical Solution of Kepler Equation

    Args:
        M: Mean anomaly
        e: eccentricity
        abstol: tolerance for convergence

    Outputs:
        E: eccentric anomaly
        v: true anomaly
        n_it: number of iterations until convergence
    """
        
    if M > 180:
        M -= 360 # M < 180 deg
    elif M < -180:
        M += 360 # M > 180 deg
        
    M = M * np.pi / 180   # to Rads

    # Initial E selection
    if M < 0:
        E = M - e
    else:
        E = M + e
                
    E0 = E - 3*TOLERANCE   # comparison value
    n_it = 0  # counter

    while np.abs(E - E0) > TOLERANCE:
            E0 = E
            E += (M - E + e*np.sin(E)) / (1 - e*np.cos(E))
            n_it += 1

    nu = np.sign(E) * np.arccos((np.cos(E)-e)/(1-e*np.cos(E)))

    return [E, nu, n_it]

def test_graphic_M_vs_E():
    M = np.linspace(-180,180,100)
    e = np.linspace(0,0.98,3)

    fig, axs = plt.subplots(2,figsize=(8,6))
    plt.subplots_adjust(hspace = 0.5)
    
    M[M > 180] -= 360
    M[M < -180] += 360

    for j in e:
        angles_Ev = []
        for m in M:
            angles_Ev += [KepEqtnE(m, j)]
        string = 'e = ' + str(j)
        angles_Ev = np.array(angles_Ev)
        axs[0].plot(angles_Ev[:,0]*180/np.pi, M, label = string, linewidth=0.8 ) # gráfica E vs M
        axs[1].plot(angles_Ev[:,1]*180/np.pi, M, label = string, linewidth=0.8 ) # gráfica v vs M
        
        print("Number of iteration for e =", j,": ", angles_Ev[:,2], "\n")
    
    axs[0].set_yticks(np.linspace(-180, 180, 13))
    axs[0].set_xticks(np.linspace(-180, 180, 13))    
    axs[0].grid()
    axs[0].set_title("M vs E (Elliptical Solution)")
    axs[0].set_ylabel("Mean anomaly")
    axs[0].set_xlabel("Eccentric anomaly")
    
    axs[1].set_yticks(np.linspace(-180, 180, 13))
    axs[1].set_xticks(np.linspace(-180, 180, 13))    
    axs[1].grid()
    axs[1].set_title("M vs v (Elliptical Solution)")
    axs[1].set_ylabel("Mean anomaly")
    axs[1].set_xlabel("True anomaly")
    axs[1].legend()
    plt.show()
    
    return 0

def getTOF(E1, E2, e, a):
    mu_earth = 398600.4415 
    return np.sqrt(a**3/mu_earth) * (E1 - e*np.sin(E1) - E2 + e*np.sin(E2))

if __name__ == "__main__":
    test_graphic_M_vs_E()