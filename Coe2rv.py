# -*- coding: utf-8 -*-
#
# Algoritmos 9 y 10 - Vallado
#
# Purpose:  Converting between position and velocity vectors to orbital elements
# Author:   Nicolás Conde
# Creation Date:  Oct. 9, 2018
#

import numpy as np

from Transformaciones import PQW2ECEF
from Parameters import MU_EARTH, TOLERANCE, Vector, COE

def RV2COE(r, v):
    """
    Converting between position and velocity vectors to orbital elements

    Args:
        r: position vector [km]
        v: velocity vector [km]
        mu: gravitational parameter of the planet # [km^3/sec^2]
        tol: tolerance for convergence

    Outputs:
        COE: orbital parameters:
            param_orb = [a, e, p, i, w, raan, nu, lamda_true, W_true, u]
                        [0, 1, 2, 3, 4,   5,  6,       7,       8,    9]
    """
    param_orb = np.zeros(10) 
    
    h = Vector(np.cross(r.coor, v.coor), "ECEF")
    n = Vector(np.cross([0,0,1], h.unit), "ECEF")
    param_orb[0] = - 0.5 * MU_EARTH / (v.abs**2/2 - MU_EARTH/r.abs)
    e = Vector(((v.abs**2 - MU_EARTH/r.abs) * r.coor - np.dot(r.coor, v.coor)*v.coor)/MU_EARTH, "ECEF")
    param_orb[1] = e.abs
    param_orb[2] = h.abs**2 / MU_EARTH
    param_orb[3] = np.arccos(np.dot([0,0,1], h.unit))
    
    param_orb[4] = np.arccos(np.dot(n.coor, e.unit))
    if e.coor[2] < TOLERANCE:
        param_orb[4] = 2*np.pi - param_orb[4]
    
    param_orb[5] = np.arccos(np.dot([1,0,0], n.unit))
    if n.coor[1] < -TOLERANCE:
        param_orb[5] = np.pi*2 - param_orb[5] 
        
    #Orbitas Ecuatoriales
    if param_orb[3] < TOLERANCE or np.pi-TOLERANCE < param_orb[3] < np.pi + TOLERANCE: 
        if e.abs < TOLERANCE:
            param_orb[7] = np.arccos(r.coor[0] / r.abs)
            print(param_orb[7])
            if r.coor[1] < -TOLERANCE:
                param_orb[7] = 2*np.pi - param_orb[7]
       
        param_orb[8] = np.arccos(np.dot([1,0,0], e.unit))
        if e.coor[1] < -TOLERANCE:
            param_orb[8] = 2*np.pi - param_orb[8]
            
    #Orbitas No Ecuatoriales        
    else: 
        if e.abs < TOLERANCE:
            param_orb[9] = np.arccos(np.dot(n.unit, r.unit))
            if r.coor[2] < -TOLERANCE:
                param_orb[9] = 2*np.pi - param_orb[9]
        else:            
            param_orb[6] = np.arccos(np.dot(e.unit, r.unit))
            if np.dot(r.coor, v.coor) < -TOLERANCE:
                param_orb[6] = 2*np.pi - param_orb[6]
     
    return COE(param_orb)
    
def COE2RV(a, e, raan, i, w, nu, u = float('nan'), l_true = float('nan'), w_true = float('nan')):
    """
    Converting between position and velocity vectors to orbital elements

    Args:
        a: semimajor axis [km]
        e: Eccentricity
        raan: raan angle [rad]
        i: inclination angle [rad]
        w: perigee argument angle [rad]
        nu: true anomaly angle [rad]
        mu: gravitational parameter of the planet # [km^3/sec^2]
        u, l_true, w_true: corrections parameters [rad]

    Outputs:
        r_vec: position vector [km}
        v_vec: velocity vector [km]
    """
    p = a * (1 - e**2)
    if abs(i) < 1e-6 or np.pi - 1e-6 < i < np.pi + 1e-6:    
        if abs(e) < TOLERANCE:
            w, raan = 0.0, 0.0
            nu = l_true
        else:
            raan = 0.0
            w = w_true
    else:
        if e < TOLERANCE:
            w = 0.0
            nu = u
            
    r_vec = PQW2ECEF(Vector(np.array([p * np.cos(nu) / (1 + e*np.cos(nu)), p * np.sin(nu) / (1 + e*np.cos(nu)), 0]), "ECEF"), w, i, raan)
    
    v_vec = PQW2ECEF(Vector(np.array([-np.sqrt(MU_EARTH / p) * np.sin(nu), np.sqrt(MU_EARTH / p) * (e + np.cos(nu)), 0]), "ECEF"), w, i, raan)
    
    return r_vec, v_vec

def test_RV2COE():
    print("Algorithm validation with example 2-5")
    Reci = Vector(np.array([6524.834, 6862.875, 6448.296]), "ECEF") # [km]
    Veci = Vector(np.array([4.901327, 5.533756, -1.976341]), "ECEF") # [km/s]

    COE = RV2COE(Reci, Veci)
    
    print("r magnitude: ", Reci.abs, " km")
    print("v magnitude: ", Veci.abs, " km/s\n")

    print("COE results:")
    print(COE.__str__())
    print("\n")

def test_COE2RV():
    print("\nAlgorithm validation with example 2-6")

    Reci, Veci = COE2RV(36126.64283480517, 0.83285, 227.89*np.pi/180, 87.87*np.pi/180, 53.38*np.pi/180, 92.335*np.pi/180)
        
    print("RV results:\n")
    print("Postion ")
    print(Reci.__str__())
    print("\nVelocity ")
    print(Veci.__str__())
    print("\n")

if __name__ == "__main__":
    test_RV2COE()
    test_COE2RV()