# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 13:01:09 2018

@author: Nico

Title: Rotation Coordinate
"""
import numpy as np

def ROT1(b):
    return np.array([[1, 0, 0], [0, np.cos(b), np.sin(b)], [0, -np.sin(b), np.cos(b)]])

def ROT2(b):
    return np.array([[np.cos(b), 0, -np.sin(b)], [0, 1, 0], [np.sin(b), 0, np.cos(b)]])

def ROT3(b):
    return np.array([[np.cos(b), np.sin(b), 0], [-np.sin(b), np.cos(b), 0], [0, 0, 1]])