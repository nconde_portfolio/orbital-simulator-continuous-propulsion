# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 00:03:44 2018

@author: Nico

Title: Corroboraciones
"""

def Argument_Array_Valid(shape, vector):
    if type(shape) == int:
        i = 0
        for v in vector:
            i += 1
        if shape == i:
            return True
        else:
            return False
    
def Reference_System_Valid(sys_ref):
    if sys_ref == 'ECEF' or \
       sys_ref == 'SEZ' or \
       sys_ref == 'ECI' or \
       sys_ref == 'ECI_t' or \
       sys_ref == 'PQW' or \
       sys_ref == 'EQW' or \
       sys_ref == 'RSW':
           return sys_ref
    else:
        return "Non-ref"
