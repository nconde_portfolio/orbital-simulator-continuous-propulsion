# -*- coding: utf-8 -*-
#
# ALgoritmo 3 - Vallado
#
# Purpose:  Solutions of Kepler’s Equation - Parabolic Solution
# Author:   Nicolás Conde
# Creation Date:  Oct. 8, 2018
#

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fsolve

from Parameters import MU_EARTH

def KepEqtnP(Dt, p):
    """
    Main algorithm - Parabolic Solution of Kepler Equation
    We can’t readily graph the parabolic and hyperbolic anomalies (as we can the eccentric anomaly) because 
    the eccentric anomaly represents an angle whereas the other two represent areas.
    We can graph these area measurements using the true anomaly and the resultant mean anomaly.

    Args:
        Dt: Delta time for calculation [sec]
        p: semiparameter [km]
        abstol: tolerance for convergence

    Outputs:
        B: parabolic anomaly [rad]
        v: true anomaly [rad]
        M: mean anomaly [rad]
    """
    Np = 2 * (MU_EARTH/p**3)**0.5

    cubic_eq = lambda B : B**3/3 + B - Np*Dt

    B_initial_guess = 0.5
    B_solution = fsolve(cubic_eq, B_initial_guess)

    nu = 2*np.arctan(B_solution)
    M = Np*Dt

    return [B_solution, nu, M]

def test_parabolic_plot():
    
    """
    Plot M vs v variation

    Outputs:
        E: eccentric anomaly [rad]
    """
    fig = plt.figure(figsize=(8,6))

    Dt = np.linspace(-180,180,360)*60
    p = 25512 #km
    B = []
    for dt in Dt:
        B += [KepEqtnP(dt, p)]
    B = np.array(B)
    plt.plot(B[:,1]*180/np.pi, B[:,2]*180/np.pi, linewidth=0.8 )
    plt.yticks(np.linspace(-180, 180, 13))   
    plt.xticks(np.linspace(-180, 180, 13))    
    plt.grid()
    plt.title("M vs v (Parabolic Solution)")
    plt.ylabel("Mean anomaly")
    plt.xlabel("True anomaly")
    plt.legend()
    plt.show()
    
    return 0

if __name__ == "__main__":
    print(KepEqtnP(53.7874*60, 25512))
    test_parabolic_plot()

