# -*- coding: utf-8 -*-
#
# Orbit propagator
#
# Purpose:  Orbit propagator - Runge Kutta
# Author:   Nicolás Conde
# Creation Date:  Oct. 10, 2018
#

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

from Parameters import Vector, MU_EARTH, TOLERANCE, REQ_EARTH

def F(X, t, params):
    mu = params[0]
    r_vec = X[:3]
    v_vec = X[3:]
    r = np.linalg.norm(r_vec)
    total_acc = - mu * r_vec / r**3
    dX = np.concatenate([v_vec, total_acc])

    return dX

def KeplerianRungeKuttaPropagator(r0, v0, tf, dt, mu=MU_EARTH, R_earth = REQ_EARTH):
    num = int(tf/dt + 1)   # steps
    time= np.linspace(0, tf, num)

    X0 = np.concatenate([r0.coor, v0.coor])
    X  = odeint(F, X0, time, args = ((mu,),), rtol = TOLERANCE, atol = TOLERANCE)

    return X

def test_propagator():
    print("Propagation of example 2-5")
    r_IJK = Vector(np.array([6524.834, 6862.875, 6448.296]), "ECEF") # [km]
    v_IJK = Vector(np.array([4.901327, 5.533756, -1.976341]), "ECEF") # [km/s]

    X = KeplerianRungeKuttaPropagator(r_IJK, v_IJK, 24*60*60, 1)

    circle1 = plt.Circle((0, 0), 6052, color='#CFCFCF')

    fig, ax = plt.subplots() 

    plt.scatter(X[:, 0], X[:,1], s = .05, c = 'r')
    plt.xlabel("X [km]")
    plt.ylabel("Y [km]")
    plt.axis('equal')
    plt.xlim(-7000, 7000)
    plt.ylim(-7000, 7000)

    ax.add_artist(circle1)

    plt.show()

    time= np.linspace(0, 24*60*60, int(24*60*60+1))

    plt.figure()
    plt.plot(time, X[:,0], label='x [km]')
    plt.plot(time, X[:,1], label='y [km]')
    plt.xlabel('t [s]')
    plt.ylabel('Coordinates [km]')
    plt.show()

if __name__ == "__main__":
    test_propagator()
