# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 12:32:03 2018

@author: Nico

Title: SPG4
"""
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv

import sys
import datetime
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from Transformaciones import PQW2ECEF
from Parameters import TOLERANCE, Vector, COE
from Coe2rv import RV2COE, COE2RV

def sgp4_prop(tle, delta_final, delta_t):
    
    satellite = twoline2rv(tle[0], tle[1], wgs72)
    
    posicion  = []
    velocidad = []
    coe       = []
    
    fecha0 = satellite.epoch # Esta en el TLE
    total_iterations = int(delta_final/delta_t)
    fecha = fecha0
    while fecha <= fecha0+delta_final:
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("[%-100s] %d%%" % ('='*int(100*(fecha-fecha0)/delta_t/total_iterations), int(100*(fecha-fecha0)/delta_t/total_iterations)))
        sys.stdout.flush()
        pos, vel  = satellite.propagate(fecha.year, fecha.month, fecha.day, fecha.hour, fecha.minute, fecha.second)
        posicion  += [pos]
        velocidad += [vel]
        coe       += [RV2COE(Vector(np.array(pos), "ECEF"), Vector(np.array(vel), "ECEF"))]
        fecha     = fecha + delta_t
        
    return np.array(posicion), np.array(velocidad), np.array(coe)

def test_sgp4_propagator():
    line1 = ('1 07276U 74026A   18301.34856347  .00000077  00000-0  57845-3 0  9990')
    line2 = ('2 07276  63.1917  84.0807 6839955 289.2997  12.2140  2.45094865216809')

    ### USO
    tle           = [line1, line2]
    delta_final   = datetime.timedelta(days=15)
    delta_t       = datetime.timedelta(seconds = 5)
    pos, vel, coe = sgp4_prop(tle, delta_final, delta_t)

    t = np.linspace(0, len(pos), len(pos))
    a = np.zeros(len(pos))
    i = 0

    for c in coe:
        a[i] = c.a
        i+=1

    plt.figure()
    plt.scatter(t/60/60/24, a, marker="o", facecolors="None", color="red", s=1, linewidths=1, label="Circles")
    plt.title("Semimajor axis variation")
    plt.xlabel("time [days]")
    plt.ylabel("a [km]")
    plt.show()

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(pos[:,0], pos[:,1], pos[:,2], s = 1, c = (0.380, 0.470, 0.514), marker = ",")
    plt.title("Orbit propagation")
    ax.set_xlabel("I [km]")
    ax.set_ylabel("J [km]")
    ax.set_zlabel("K [km]")
    plt.show()

if __name__ == "__main__":
    test_sgp4_propagator()