# -*- coding: utf-8 -*-
#
# Coordinates transformation
#
# Purpose:  Classic transformations between coordinate reference systems
# Author:   Nicol�s Conde
# Creation Date:  Oct. 9, 2018
#
import numpy as np

def t2JD(t):
    '''Transforms time (datetime) to Julian Date.'''
    return 367*t.year - int(7*(t.year+int((t.month+9)/12))/4) + int(275*t.month/9) + t.day + 1721013.5 + t.hour/24 + t.minute/24/60 + t.second/24/60/60 + t.microsecond/24/60/60/1000000


def TJ2000(jd):
    '''Transforms Julian time to Julian 2000 time.'''
    return (jd - 2451545)/36525


def LSTtime(UT1, longitude):
    '''
    Gets the GMST angle from the Julian 2000 time (for high precision use UT1 in julian 
    format, the difference with UTC is always less than a second so it works as well)
    
    Algorithm 15
    '''
    JDut1 = t2JD(UT1)
    Tut1 = (JDut1 - 2451545.0) / 36525.0
    th = 100.4606184 + 36000.77005361*Tut1 + 0.00038793*Tut1**2 - 2.6e-8*Tut1**3
    w = 1.002737909350795 * 2*np.pi / (24*3600)
    th = np.deg2rad(th)
    Ogmst = th + w*(UT1.hour*3600 + UT1.minute*60 + UT1.second)
    Ogmst = Ogmst%(2*np.pi)
    return Ogmst, Ogmst+longitude

def DMS2RAD(DMS):
    """
    convert angles expressed in a degree-arcminute-arcsecond (DMS) to one in radians

    Args:
        DMS: degree-arcminute-arcsecond

    Output:
        Angle in radians
    """
    return (DMS[0] + DMS[1]/60 + DMS[2]/3600)*np.pi/180

def RAD2DMS(RAD):
    """
    convert angle in radians to one expressed in a degree-arcminute-arcsecond (DMS)

    Args:
        RAD: Angle in radians

    Output:
        DMS: degree-arcminute-arcsecond
    """
    temp = RAD * 180 / np.pi
    min  = temp%1 * 60
    sec  = min%1 * 3600
    return np.array([int(temp),int(min),sec])

def HMS2RAD(HMS):
    """
    convert times in an hour-minute-second (HMS) format to one in radians

    Args:
        HMS: hour-minute-second

    Output:
        times in radians
    """
    return 15*(HMS[0] + HMS[1]/60 + HMS[2]/3600)*np.pi/180

def RAD2HMS(t_rads):
    """
    convert times in radians to one expressed in an hour-minute-second (HMS) format

    Args:
        RAD: time in radians

    Output:
        HMS: hour-minute-second
    """
    temp = t_rads * 180 / 15 / np.pi
    h  = int(temp)
    min  = temp%1 * 60
    sec  = min%1 * 60
    return np.array([int(temp),int(min),sec])

def JD2GregorianDate(JD):
    """
    find the Gregorian calendar date

    Args:
        JD: Julian Date

    Output:
        GD: Gregorian calendar date
    """
    LMonth = np.array([31,28,31,30,31,30,31,31,30,31,30,31])

    T1900 = (JD - 2415019.5)/365.25
    year  = 1900 + int(T1900)
    leap_years = int((year-1900-1)*0.25)
    days = JD - 2415019.5 - (year-1900)*365.0 - leap_years

    if days < 1.0:
        year -= 1
        leap_years = int((year-1900-1)*0.25)
        days = JD - 2415019.5 - (year-1900)*365.0 - leap_years

    if year%4==0:
        Lmonth[1] = 29

    month     = (np.cumsum(LMonth)+1 < int(days)).argmin()+1
    sum_month = LMonth[:month-1].sum()

    day = int(days) - sum_month
    h   = days%1 * 24
    min = h%1 * 60
    sec = min%1 * 60

    return [year, month, day, int(h), int(min), sec]

def test_rad2hms():
    print("\n\nTest RAD to HMS\n")
    hms = np.array([15,15,53.63])
    rad = HMS2RAD(hms)
    print(rad)
    print(RAD2HMS(rad))

def test_rad2dms():
    print("\n\nTest RAD to DMS\n")
    dms = np.array([-35,-15,-53.63])
    rad = DMS2RAD(dms)
    print(RAD2DMS(rad))

def test_JD():
    print("\n\nTest Datetime to JD\n")
    import datetime
    t = datetime.datetime(1996, 10, 26, 14, 20, 0)
    print("Julian Date: ", t2JD(t))

def test_example3_5():
    print("\n\nTest validation example 3-5\n")
    import datetime
    t_ut1 = datetime.datetime(1992, 8, 20, 12, 14, 0)
    
    print("Julian Date: ", np.array(LSTtime(t_ut1, -104*np.pi/180))*180/np.pi)

def test_JD2GD():
    print("\n\nValidation with example 3-13\n")
    print(JD2GregorianDate(2449877.3458762))

if __name__ == "__main__":
    test_rad2hms()
    test_rad2dms()
    test_JD()
    test_example3_5()
    test_JD2GD()